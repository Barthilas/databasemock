﻿using DatabaseMock.Commands;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using DatabaseMock.Model;

namespace DatabaseMock.ViewModel
{
    class MainWindowViewModel : ViewModelBase
    {
        private int _numOfEntries;
        public int numOfEntries
        {
            get
            {
                return _numOfEntries;
            }
            set
            {
                _numOfEntries = value;
                NotifyPropertyChanged();
            }
        }
        public string _status { get; set; }
        public string STATUS
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                NotifyPropertyChanged();
            }
        }
        public SimpleCommand FillDB1Command { get; set; }
        public SimpleCommand FillDB2Command { get; set; }
        public SimpleCommand FillDB3Command { get; set; }
        public SimpleCommand FillDB4Command { get; set; }
        public SimpleCommand FillDB5Command { get; set; }
        public SimpleCommand FillDB6Command { get; set; }
        public SimpleCommand FillDB7Command { get; set; }

        public MainWindowViewModel()
        {
            numOfEntries = 10;
            STATUS = "OFFLINE";
            FillDB1Command = new SimpleCommand(FillUnitSettingDB);
            FillDB2Command = new SimpleCommand(FillUnit);
            FillDB3Command = new SimpleCommand(FillMachineSetting);
            FillDB4Command = new SimpleCommand(FillMachine);
            FillDB5Command = new SimpleCommand(FillTensiometer);
            FillDB6Command = new SimpleCommand(FillData);
            FillDB7Command = new SimpleCommand(FillBrainDead);

        }

        public async void FillBrainDead()
        {
            STATUS = "Computing...";
            await Task.Run(ExecuteFillUnitSetting);
            await Task.Run(ExecuteFillUnit);
            await Task.Run(ExecuteFillMachineSetting);
            await Task.Run(ExecuteFillMachine);
            await Task.Run(ExecuteFillTensiometer);
            await Task.Run(ExecuteFillData);
            STATUS = "Braindead fill complete";
        }

        public async void FillData()
        {
            STATUS = "Filling Data";
            await Task.Run(() => ExecuteFillData());
            STATUS = "Filling complete";
        }

        public async void FillTensiometer()
        {
            STATUS = "Filling Tensiometer";
            await Task.Run(() => ExecuteFillTensiometer());
            STATUS = "Filling complete";
        }

        public async void FillUnitSettingDB()
        {
            STATUS = "Filling UnitSetting";
            await Task.Run(() => ExecuteFillUnitSetting());
            STATUS = "Filling complete";
        }
        public async void FillUnit()
        {
            STATUS = "Filling Unit";
            await Task.Run(() => ExecuteFillUnit());
            STATUS = "Filling complete";
        }
        public async void FillMachineSetting()
        {
            STATUS = "Filling MachineSetting";
            await Task.Run(() => ExecuteFillMachineSetting());
            STATUS = "Filling complete";
        }
        public async void FillMachine()
        {
            STATUS = "Filling Machine";
            await Task.Run(() => ExecuteFillMachine());
            STATUS = "Filling complete";
        }
        async Task ExecuteFillData()
        {
            Trace.WriteLine("Fill Data");
            var con = Database.Singleton.GetConnection;

            var fk_Machine = GetForeignKeysAsString("VIN", "Machine");
            var fk_MachineSetting = GetForeignKeys("MachineSettingID", "MachineSetting");
            var fk_Unit = GetForeignKeys("UnitSerialNumber", "Unit");
            var fk_UnitSetting = GetForeignKeys("UnitSettingID", "UnitSetting");

            for (int i = 0; i < numOfEntries; i++)
            {
                int a = Generator.random.Next(fk_Machine.Count);
                int b = Generator.random.Next(fk_MachineSetting.Count);
                int c = Generator.random.Next(fk_Unit.Count);
                int d = Generator.random.Next(fk_UnitSetting.Count);

                using var cmd2 = new SQLiteCommand(con);
                {
                    cmd2.CommandText = @"INSERT INTO Data(MachineVIN, MachineSettingID, UnitSerialNumber, UnitSettingID,
                    DateTime, State, GPSlat, GPSlon, Battery, AxcelerometerX, AxcelerometerY, AxcelerometerZ, Tensiometer1,
                    Tensiometer2,Tensiometer3, Tensiometer4, Tensiometer5)
                    VALUES(@1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, @17)";

                    cmd2.Parameters.Add("@1", System.Data.DbType.String).Value = fk_Machine[a];
                    cmd2.Parameters.AddWithValue("@2", fk_MachineSetting[b]);
                    cmd2.Parameters.AddWithValue("@3", fk_Unit[c]);
                    cmd2.Parameters.AddWithValue("@4", fk_UnitSetting[d]);
                    cmd2.Parameters.AddWithValue("@5", DateTimeSQLite(DateTime.Now));
                    cmd2.Parameters.AddWithValue("@6", "MOCK");
                    cmd2.Parameters.AddWithValue("@7", Generator.GetDouble(-90,90)); //lat
                    cmd2.Parameters.AddWithValue("@8", Generator.GetDouble(-180,80)); //lon
                    cmd2.Parameters.AddWithValue("@9", Generator.GetInt(0, 100)); //bat
                    cmd2.Parameters.AddWithValue("@10", Generator.GetInt(0, 100)); //AxceletometerX m/s
                    cmd2.Parameters.AddWithValue("@11", Generator.GetInt(0, 100)); //AxcelerometerY
                    cmd2.Parameters.AddWithValue("@12", Generator.GetInt(0, 100)); //AxcelerometerZ
                    cmd2.Parameters.AddWithValue("@13", Generator.GetInt(800,10000)); //T1
                    cmd2.Parameters.AddWithValue("@14", Generator.GetInt(800, 10000)); //T2
                    cmd2.Parameters.AddWithValue("@15", Generator.GetInt(800, 10000)); //T3
                    cmd2.Parameters.AddWithValue("@16", Generator.GetInt(800, 10000)); //T4
                    cmd2.Parameters.AddWithValue("@17", Generator.GetInt(800, 10000)); //T5
                    cmd2.Prepare();

                    cmd2.ExecuteNonQuery();
                }
            }
        }
        private long DateTimeSQLite(DateTime datetime)
        {
            DateTime dateTime = DateTime.Now;
            long unixTimestamp = ((DateTimeOffset)dateTime).ToUnixTimeSeconds();
            return unixTimestamp;
            //string dateTimeFormat = "{0}-{1}-{2} {3}:{4}:{5}.{6}";
            //return string.Format(dateTimeFormat, datetime.Year, datetime.Month, datetime.Day, datetime.Hour, datetime.Minute, datetime.Second, datetime.Millisecond);
        }
        async Task ExecuteFillTensiometer()
        {
            Trace.WriteLine("Fill Tensiometer");
            var con = Database.Singleton.GetConnection;

            var fk_Machine = GetForeignKeysAsString("VIN", "Machine");

            for (int i = 0; i < numOfEntries; i++)
            {
                int index_fk = Generator.random.Next(fk_Machine.Count);
                using var cmd2 = new SQLiteCommand(con);
                {
                    cmd2.CommandText = @"INSERT INTO Tensiometer(MachineVIN, IsActive, Note) VALUES(@vin, @active, @note)";

                    cmd2.Parameters.Add("@vin", System.Data.DbType.String).Value = fk_Machine[index_fk];
                    cmd2.Parameters.AddWithValue("@active", Generator.GetBoolean());
                    cmd2.Parameters.AddWithValue("@note", "GENERATED WITH MOCK");
                    cmd2.Prepare();

                    cmd2.ExecuteNonQuery();
                }
            }
        }
        private List<int> GetForeignKeys(string what, string table)
        {
            var con = Database.Singleton.GetConnection;
            List<int> ID = new List<int>();
            string stm = string.Format("SELECT DISTINCT {0} FROM {1}", what, table);
            using var cmd = new SQLiteCommand(con);
            cmd.CommandText = stm;
            using SQLiteDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                ID.Add(rdr.GetInt32(0));
                Trace.WriteLine($"{rdr.GetInt32(0)}");
            }
            return ID;
        }
        private List<string> GetForeignKeysAsString(string what, string table)
        {
            var con = Database.Singleton.GetConnection;
            List<string> ID = new List<string>();
            string stm = string.Format("SELECT DISTINCT {0} FROM {1}", what, table);
            using var cmd = new SQLiteCommand(con);
            cmd.CommandText = stm;
            using SQLiteDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                ID.Add(rdr.GetString(0));
                Trace.WriteLine($"{rdr.GetString(0)}");
            }
            return ID;
        }
        async Task ExecuteFillMachine()
        {
            Trace.WriteLine("Fill Machine");
            var con = Database.Singleton.GetConnection;

            var fk_MachineSetting = GetForeignKeys("MachineSettingID", "MachineSetting");
            var fk_Unit = GetForeignKeys("UnitSerialNumber", "Unit");

            for (int i = 0; i < numOfEntries; i++)
            {
                int index_fk1 = Generator.random.Next(fk_MachineSetting.Count);
                int index_fk2 = Generator.random.Next(fk_Unit.Count);
                using var cmd2 = new SQLiteCommand(con);
                cmd2.CommandText = "INSERT INTO Machine(VIN, UnitSN, MachineSettingID) VALUES(@vin, @fkUnit, @fkMachine)";

                cmd2.Parameters.AddWithValue("@vin", Generator.GetString(17));
                cmd2.Parameters.AddWithValue("@fkUnit", fk_Unit[index_fk2]);
                cmd2.Parameters.AddWithValue("@fkMachine", fk_MachineSetting[index_fk1]);
                cmd2.Prepare();

                cmd2.ExecuteNonQuery();
            }
        }
        async Task ExecuteFillMachineSetting()
        {
            var rand = new Random();
            Trace.WriteLine("Enter ExecuteAsync");
            var con = Database.Singleton.GetConnection;
            for (int i = 0; i < numOfEntries; i++)
            {
                using var cmd = new SQLiteCommand(con);
                cmd.CommandText = "INSERT INTO MachineSetting(SendState, SendGPS, SendAxcelerometer, SendTensiometer) VALUES(@state, @GPS, @ax, @te)";

                cmd.Parameters.AddWithValue("@state", Generator.GetBoolean());
                cmd.Parameters.AddWithValue("@GPS", Generator.GetBoolean());
                cmd.Parameters.AddWithValue("@ax", Generator.GetBoolean());
                cmd.Parameters.AddWithValue("@te", Generator.GetBoolean());
                cmd.Prepare();
                cmd.ExecuteNonQuery();
            }
            Trace.WriteLine("Exit ExecuteAsync");
        }
        async Task ExecuteFillUnitSetting()
        {
            var rand = new Random();
            Trace.WriteLine("Enter ExecuteAsync");
            var con = Database.Singleton.GetConnection;
            for (int i = 0; i < numOfEntries; i++)
            {
                using var cmd = new SQLiteCommand(con);
                cmd.CommandText = "INSERT INTO UnitSetting(Interval, SendBatteryState) VALUES(@interval, @state)";

                cmd.Parameters.AddWithValue("@interval", Generator.GetInt(1,3600));
                cmd.Parameters.AddWithValue("@state", Generator.GetBoolean());
                cmd.Prepare();

                cmd.ExecuteNonQuery();
            }
            Trace.WriteLine("Exit ExecuteAsync");
        }
        async Task ExecuteFillUnit()
        {
            Trace.WriteLine("Fill Unit");
            var random = new Random();
            var con = Database.Singleton.GetConnection;

            //get all available Ids
            List<int> Ids = GetForeignKeys("UnitSettingID", "UnitSetting");
            
            for (int i = 0; i < numOfEntries; i++)
            {
                int index = random.Next(Ids.Count);
                using var cmd2 = new SQLiteCommand(con);
                cmd2.CommandText = "INSERT INTO Unit(UnitSettingID) VALUES(@fk)";

                cmd2.Parameters.AddWithValue("@fk", Ids[random.Next(Ids.Count)]);
                cmd2.Prepare();

                cmd2.ExecuteNonQuery();
            }
        }
    }
}
