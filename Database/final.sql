-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2020-05-04 09:26:15.61

-- tables
-- Table: Data
CREATE TABLE Data (
    ID INTEGER NOT NULL CONSTRAINT Data_pk PRIMARY KEY AUTOINCREMENT,
    MachineVIN varchar(17) NOT NULL,
    MachineSettingID int NOT NULL,
    UnitSerialNumber int NOT NULL,
    UnitSettingID int NOT NULL,
    DateTime long NOT NULL,
    State varchar(10),
    GPSlat decimal(2,6) DEFAULT 0,
    GPSlon decimal(2,6) DEFAULT 0,
    Battery decimal(2,2) DEFAULT 0,
    AxcelerometerX decimal(2,2) DEFAULT 0,
    AxcelerometerY decimal(2,2) DEFAULT 0,
    AxcelerometerZ decimal(2,2) DEFAULT 0,
    Tensiometer1 decimal(2,2) DEFAULT 0,
    Tensiometer2 decimal(2,2) DEFAULT 0,
    Tensiometer3 decimal(2,2) DEFAULT 0,
    Tensiometer4 decimal(2,2) DEFAULT 0,
    Tensiometer5 decimal(2,2) DEFAULT 0,
    CONSTRAINT Data_Unit FOREIGN KEY (UnitSerialNumber)
    REFERENCES Unit (UnitSerialNumber),
    CONSTRAINT Data_Machine FOREIGN KEY (MachineVIN)
    REFERENCES Machine (VIN)
    ON UPDATE CASCADE,
    CONSTRAINT Data_UnitSetting FOREIGN KEY (UnitSettingID)
    REFERENCES UnitSetting (UnitSettingID)
    ON UPDATE CASCADE,
    CONSTRAINT Data_MachineSetting FOREIGN KEY (MachineSettingID)
    REFERENCES MachineSetting (MachineSettingID)
    ON UPDATE CASCADE
);

-- Table: Machine
CREATE TABLE Machine (
    VIN varchar(17) NOT NULL CONSTRAINT Machine_pk PRIMARY KEY UNIQUE,
    UnitSN int NOT NULL,
    MachineSettingID int NOT NULL,
    CONSTRAINT Unit_Machine FOREIGN KEY (UnitSN)
    REFERENCES Unit (UnitSerialNumber)
    ON UPDATE CASCADE,
    CONSTRAINT Machine_Setting FOREIGN KEY (MachineSettingID)
    REFERENCES MachineSetting (MachineSettingID)
    ON UPDATE CASCADE
);

-- Table: MachineSetting
CREATE TABLE MachineSetting (
    MachineSettingID INTEGER NOT NULL CONSTRAINT MachineSetting_pk PRIMARY KEY AUTOINCREMENT,
    SendState bool NOT NULL,
    SendGPS bool NOT NULL,
    SendAxcelerometer bool NOT NULL,
    SendTensiometer bool NOT NULL
);

-- Table: Tensiometer
CREATE TABLE Tensiometer (
    TensiometerID INTEGER NOT NULL CONSTRAINT Tensiometer_pk PRIMARY KEY AUTOINCREMENT,
    MachineVIN varchar(17) NOT NULL,
    IsActive boolean NOT NULL,
    Note varchar(30),
    CONSTRAINT Machine_Tensiometer FOREIGN KEY (MachineVIN)
    REFERENCES Machine (VIN)
    ON UPDATE CASCADE
);

-- Table: Unit
CREATE TABLE Unit (
    UnitSerialNumber INTEGER NOT NULL CONSTRAINT Unit_pk PRIMARY KEY AUTOINCREMENT,
    UnitSettingID int NOT NULL,
    CONSTRAINT Unit_UnitSetting FOREIGN KEY (UnitSettingID)
    REFERENCES UnitSetting (UnitSettingID)
    ON UPDATE CASCADE
);

-- Table: UnitSetting
CREATE TABLE UnitSetting (
    UnitSettingID INTEGER NOT NULL CONSTRAINT UnitSetting_pk PRIMARY KEY AUTOINCREMENT,
    Interval int NOT NULL,
    SendBatteryState boolean NOT NULL
);

-- End of file.

