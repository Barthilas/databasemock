﻿using System;

namespace DatabaseMock.Commands
{
    public class SimpleCommand : BaseCommand
    {
        protected Action _execute;

        public SimpleCommand(Action execute)
            : this(execute, DefaultCanExecute)
        {
        }

        public SimpleCommand(Action execute, Predicate<object> canExecute)
            : base(canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException("Vlastnost Execute není nastavena.");
            }

            this._execute = execute;
        }

        override public void Execute(object parameter)
        {
            //System.Windows.MessageBox.Show(_execute.Method.Name);
            this._execute();
        }

        public void Destroy()
        {
            this._canExecute = _ => false;
            this._execute = () => { return; };
        }
    }
}
