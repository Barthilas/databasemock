﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseMock.Model
{
    public static class Generator
    {
        public static Random random = new Random();

        /// <summary>
        /// from min to max (fixed in method -> Exact number)
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int GetInt(int min, int max)
        {
            max = max + 1;
            var number = random.Next(min, max);
            return number;
        }
        public static double GetDouble(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }
        public static int GetBoolean()
        {
            return random.Next(0, 2);
        }
        public static string GetString(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var stringChars = new char[length];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new string(stringChars);
            return finalString;
        }
    }
}
