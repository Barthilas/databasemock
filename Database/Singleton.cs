﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.IO;

namespace DatabaseMock.Database
{
    public sealed class Singleton
    {
        public static readonly string db_path = "db.db";
        public static string projectBin = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;
        public static string projectRoot = Path.GetFullPath(Path.Combine(projectBin, @"..\"));
        private static SQLiteConnection connection;

        private Singleton() 
        {
        }

        public static SQLiteConnection GetConnection
        {
            get
            {
                if (connection == null)
                {
                    connection = CreateConnection();
                }
                return connection;
            }
        }
        private static SQLiteConnection CreateConnection()
        {
            var dbPath = Path.Combine(projectRoot, "Database", db_path);
            var connectionString = string.Format("Data Source = {0}; Version = 3;", dbPath);
            var con = new SQLiteConnection(connectionString);
            con.Open();
            return con;
        }

    }
}
